// simple throttle function
// inspired by https://stackoverflow.com/questions/27078285/simple-throttle-in-js
export function throttle(callback, timeLimit) {
  let waiting = false
  return function(...args) {
    if (!waiting) {
      callback.apply(this, args)
      waiting = true
      setTimeout(() => {
        waiting = false
      }, timeLimit)
    }
  }
}

// inspired by https://gist.github.com/Daniel-Hug/abbded91dd55466e590b
export function delegate(element, event, descendentSelector, callback){
  element.addEventListener(event, function(e){
    const elem = e.target.closest(descendentSelector)
    // returns null if no matching parentNode is found
    if (elem) {
      callback.call(elem, e)
    }
  }, true);
}

export const symbolPath = new Path2D('M150 57A117 117 0 1 1 33 173 117 117 0 0 1 150 57Z')
symbolPath.addPath(new Path2D('M150 13l140 242H10Z'))
symbolPath.addPath(new Path2D('M41 168L150 59 259 168 150 277Z'))
symbolPath.addPath(new Path2D('M73 136l77-77 77 77-77 77Z'))
symbolPath.addPath(new Path2D('M104 105l46-46 46 46-46 46Z'))

export const hoursPath = new Path2D()
hoursPath.moveTo(-30, -8)
hoursPath.lineTo(85, -5)
hoursPath.lineTo(85, 5)
hoursPath.lineTo(-30, 8)
hoursPath.closePath()

export const minutesPath = new Path2D()
minutesPath.moveTo(-30, -8)
minutesPath.lineTo(121, -4)
minutesPath.lineTo(121, 4)
minutesPath.lineTo(-30, 8)
minutesPath.closePath()

export const secondsPath = new Path2D()
secondsPath.moveTo(-40, -2.5)
secondsPath.lineTo(85, -1.5)
secondsPath.lineTo(85, 1.5)
secondsPath.lineTo(-40, 2.5)
secondsPath.closePath()
secondsPath.arc(0, 0, 5, 0, Math.PI * 2)
secondsPath.closePath()
secondsPath.arc(85, 0, 10, 0, Math.PI * 2)
secondsPath.closePath()


export function drawClockFrame(ctx, ctxW = 300, ctxH = 300, w = 300) {
  const radius = w / 2
  const frameWidth = 15
  const frameMargin = frameWidth + 12
  const hoursSize = 30
  const hoursWidth = hoursSize/3
  const minutesSize = hoursSize/3
  const minutesWidth = minutesSize/3

  ctx.save()

  ctx.strokeStyle = '#000'

  // Hour marks
  ctx.save()
  ctx.lineWidth = hoursWidth
  for (let i = 0; i < 12; i++) {
    ctx.beginPath()
    ctx.rotate(Math.PI / 6)
    ctx.moveTo(radius - frameMargin - hoursSize, 0)
    ctx.lineTo(radius - frameMargin, 0)
    ctx.stroke()
  }
  ctx.restore()

  // Minute marks
  ctx.save()
  ctx.lineWidth = minutesWidth
  for (let i = 0; i < 60; i++) {
    if (i % 5!= 0) {
      ctx.beginPath()
      ctx.moveTo(radius - frameMargin - minutesSize, 0)
      ctx.lineTo(radius - frameMargin, 0)
      ctx.stroke()
    }
    ctx.rotate(Math.PI / 30)
  }
  ctx.restore()

  // frame
  ctx.beginPath()
  ctx.lineWidth = frameWidth
  ctx.strokeStyle = '#969694'
  const frameRadius = (w - frameWidth) / 2
  ctx.arc(0, 0, frameRadius, 0, Math.PI * 2, true)
  ctx.stroke()

  ctx.restore()
}

const getTimeParts = (date) => {
  return {
    h: date.getHours(),
    m: date.getMinutes(),
    s: date.getSeconds(),
    ms: date.getMilliseconds(),
  }
}

/**
 * get time parts rotations
 * tries to imitate the know stop2go feature
 * @param {Date} date the date from which to get the rotations
 */
export function getTimeRotations (date) {
  const { h, m, s, ms } = getTimeParts(date)

  const pi2 = Math.PI * 2

  const secondsInAMinute = 60
  const secondsPause = 1.5
  const minutesAnimationDuration = secondsPause / 10
  const animationRatio = minutesAnimationDuration / secondsInAMinute
  const secondsCorrected = secondsInAMinute - secondsPause
  const animationStartRatio = (secondsInAMinute - minutesAnimationDuration) / secondsInAMinute

  const msRatio = ms / 1000
  const sRatio = (s + msRatio) / secondsInAMinute
  // stop2go pauses 2 seconds every minute
  // therefore the second needle performs a full revolution in 60 seconds minus the pause time
  // it will then remain static for the pause duration
  const sRatioCorr = Math.min((s + msRatio) / secondsCorrected, 1)
  // the minute needle is always exactly centered on a minute
  // except during the small gap where it is animated
  const pauseMRatio = (sRatio - animationStartRatio) / animationRatio
  const mRatio = sRatio < animationStartRatio
    ? m / 60
    : (m + pauseMRatio) / 60
  const hRatio = (h + mRatio) / 12

  const sRotation = pi2 * sRatioCorr
  const mRotation = pi2 * mRatio
  const hRotation = pi2 * hRatio

  return {
    h: hRotation,
    m: mRotation,
    s: sRotation,
  }
}

export function setBtnListeners () {
  delegate(document, 'click', '.canvas-dl', function () {
    const btn = this
    const canvas = btn.closest('.exercise-step').querySelector('canvas')
    if (canvas) {
      const canvasURL = canvas.toDataURL('image/png').replace('image/png', 'image/octet-stream')
      btn.setAttribute('href', canvasURL)
    }
  })
}
