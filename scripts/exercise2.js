import { symbolPath, getTimeRotations, drawClockFrame, hoursPath, minutesPath, secondsPath, setBtnListeners } from './helpers.js'

(function exercise1() {
  /**
   * Dessinons sur ces canvas.
   * 
   * 1. Tenter de reproduire le rendu de l'image d'exemple
   *  a. utiliser le tracé importé symbolPath
   *  b. corriger l'erreur de proportions
   *  c. répliquer le style de l'image
   * 
   * 2. Tenter de reproduire le rendu de l'image d'exemple
   *  a. utiliser une boucle : l'image comporte 2 x 12 carrés
   *  b. utiliser la transformation du contexte
   *     pour tourner le canvas sur lui-même à son centre, il faut:
   *       - déplacer l'origine du canvas à son centre
   *       - effectuer la rotation
   *       - ramener l'origin du canvas à son point de départ (mouvement inverse de l'avant-dernier point)
   * 
   * 3. Tenter de reproduire le rendu de l'image d'exemple
   *  a. commencer par dessiner un symbole (utiliser un Path2d, réutilisable)
   *  b. le symbole est plus complexe qu'un rectangle, utiliser des lignes
   *  c. utiliser des boucles imbriquées pour la répétition : l'image compore 15 x 15 symboles
   *  d. utiliser la transformation du contexte pour le déplacement
   * 
   * 4. Tenter de reproduire le rendu de l'image d'exemple
   *  a. dessiner les aiguilles de la montre (soi-même ou utiliser les tracés importés: hoursPath, minutesPath, secondsPath)
   *  b. mettre les aiguilles à l'heure de la variable time de la fonction drawClock
   *       - utiliser la transformation du contexte pour la rotation
   *       - utiliser les angles de rotation fournis par le helper getTimeRotations(time)
   *       - utiliser les méthodes save et restore du contexte pour retrouver l'état du canvas avant les "rotations temporelles"
   *  c. animer la montre
   *       - utiliser l'heure actuelle : new Date()
   *       - utiliser à choix :
   *           - la méthode window.requestAnimationFrame, plus efficace ~60fps
   *           - la méthode setTimeout, plus réaliste (une vraie montre tourne à ~10fps)
   * 
   * Bonus
   * 1. rajouter l'ombre aux aiguilles de la montre
   * 2. rajouter une marque à la montre
   * 3. animer l'image 3 selon son goût
   */



  // TODO #1
  const canvas1 = document.querySelector('#canvas-1')
  /** @type {CanvasRenderingContext2D} */
  const ctx1 = canvas1.getContext('2d')



  // TODO #2
  const canvas2 = document.querySelector('#canvas-2')
  const canvas2Width = canvas2.width
  const canvas2Height = canvas2.height
  /** @type {CanvasRenderingContext2D} */
  const ctx2 = canvas2.getContext('2d')

  ctx2.strokeRect(50, 50, 200, 200)
  ctx2.fillRect(85, 85, 130, 130)



  // TODO #3
  const canvas3 = document.querySelector('#canvas-3')
  const canvas3Width = canvas3.width
  const canvas3Height = canvas3.height
  /** @type {CanvasRenderingContext2D} */
  const ctx3 = canvas3.getContext('2d')

  const miniPath = new Path2D()
  miniPath.rect(0, 0, 10, 10)

  ctx3.fill(miniPath)



  // TODO #4
  const canvas4 = document.querySelector('#canvas-4')
  const canvas4Width = canvas4.width
  const canvas4Height = canvas4.height
  /** @type {CanvasRenderingContext2D} */
  const ctx4 = canvas4.getContext('2d')

  const drawClock = function() {
    const time = new Date()
    // Watchmaker hour
    time.setHours(10)
    time.setMinutes(9)
    time.setSeconds(36)
    time.setMilliseconds(250)

    ctx4.save()

    // move origin to center of canvas
    ctx4.translate(canvas4Width/2, canvas4Height/2)
    // rotate -90°
    ctx4.rotate(-Math.PI/2)

    // draw base frame
    drawClockFrame(ctx4)

    const { h:hRotation, m:mRotation, s:sRotation } = getTimeRotations(time)


    // draw logic HERE :-)


    ctx4.restore()

  }

  drawClock()

  // SETUP
  setBtnListeners()

})()